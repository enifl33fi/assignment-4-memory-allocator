#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define BLOCK_MIN_CAPACITY 24

void debug_block(struct block_header *b, const char *fmt, ...);
void debug(const char *fmt, ...);
extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);
extern inline bool region_is_invalid(const struct region *r);

/*  --- ... ecли размера кучи хватает --- */
struct block_search_result
{
  enum
  {
    BSR_FOUND_GOOD_BLOCK,
    BSR_REACHED_END_NOT_FOUND,
    BSR_CORRUPTED
  } type;
  struct block_header *block;
};

/* Checking if block is big enough to contain given data size */
static bool block_is_big_enough(size_t query, struct block_header *block) {
  return block->capacity.bytes >= query;
}

/* Checking if block is free and big enough */
static bool block_can_contain(size_t query, struct block_header *block) {
  return block->is_free && block_is_big_enough(query, block);
}

/* Getting page count that are suitable for containing given data size */
static size_t pages_count(size_t mem) {
  return mem / getpagesize() + ((mem % getpagesize()) > 0);
}

/* Getting pages size */
static size_t round_pages(size_t mem) {
  return getpagesize() * pages_count(mem);
}

/* Initialization of a block with given size and the next element at the provided address */
static void block_init(void *restrict addr, block_size block_sz, void *restrict next)
{
  *((struct block_header *)addr) = (struct block_header){
      .next = next,
      .capacity = capacity_from_size(block_sz),
      .is_free = true};
}

/* Handling region min size */
static size_t region_actual_size(size_t query) {
  return size_max(round_pages(query), REGION_MIN_SIZE);
}

/* Incapsulation of mmap */
static void *map_pages(void const *addr, size_t length, int additional_flags)
{
  return mmap((void *)addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query)
{
  size_t alloc_size = region_actual_size(size_from_capacity((block_capacity){query}).bytes);
  void* alloc_addr = map_pages(addr, alloc_size, MAP_FIXED_NOREPLACE);
  if (alloc_addr == MAP_FAILED) {
    alloc_addr = map_pages(addr, alloc_size, 0);
    if (alloc_addr == MAP_FAILED) {
      return REGION_INVALID;
    }
  }

  block_init(alloc_addr, (block_size){alloc_size}, NULL);

  bool alloc_extends = alloc_addr == addr;
  return (struct region) {
    .addr = alloc_addr,
    .size = alloc_size,
    .extends = alloc_extends
  };
}

static void *block_after(struct block_header const *block);


static bool blocks_continuous(
    struct block_header const *fst,
    struct block_header const *snd);

/* Heap initialization */
void *heap_init(size_t initial)
{
  const struct region region = alloc_region(HEAP_START, initial);
  if (region_is_invalid(&region))
    return NULL;

  return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term()
{
  struct block_header* start_elem = (struct block_header*) HEAP_START;
  struct block_header* cur_elem = start_elem;
  while (cur_elem) {
    start_elem = cur_elem;
    size_t clear_size = size_from_capacity(cur_elem->capacity).bytes;
    while(blocks_continuous(cur_elem, cur_elem->next)) {
      clear_size += size_from_capacity(cur_elem->next->capacity).bytes;
      cur_elem = cur_elem->next;
    }
    cur_elem = cur_elem->next;
    munmap(start_elem, clear_size);
  }
}

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */
static bool block_splittable(struct block_header *restrict block, size_t query)
{
  return block->is_free && query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query)
{
  if (block && block_splittable(block, query)) {
    block_size first_size = size_from_capacity((block_capacity){query});
    block_size second_size = (block_size){size_from_capacity(block->capacity).bytes - first_size.bytes};

    void* second_pointer = (uint8_t*)block + first_size.bytes;

    block_init(second_pointer, second_size, block->next);
    block_init(block, first_size, second_pointer);
    return true;
  }
  return false;
}

/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block)
{
  return (void *)(block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
    struct block_header const *fst,
    struct block_header const *snd)
{
  return (void *)snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd)
{
  return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block)
{
  if (block && block->next && mergeable(block, block->next)) {
    block_size new_size = (block_size){size_from_capacity(block->capacity).bytes + size_from_capacity(block->next->capacity).bytes};
    block_init(block, new_size, block->next->next);
    return true;
  }
  return false;
}

/* merge all blocks after given */
static void merge_all(struct block_header *block) {
  while (try_merge_with_next(block)){}
}

static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz)
{
  if (!block) {
    /* when BSR_CORRUPTED is happened? As far as I know we don't trying to handle any exception*/
    /* so let's say it kinda tells us that whole struct doesn't exist :) */ 
    return (struct block_search_result){
      .type = BSR_CORRUPTED,
      .block = block
    };
  }

  struct block_header* cur_block = block;
  struct block_header* prev_block = NULL;
  while (cur_block)
  {
    merge_all(cur_block);
    if (block_can_contain(sz, cur_block)) {
      return (struct block_search_result){
        .type = BSR_FOUND_GOOD_BLOCK,
        .block = cur_block
      };
    }
    prev_block = cur_block;
    cur_block = cur_block->next;

  }

  return (struct block_search_result){
        .type = BSR_REACHED_END_NOT_FOUND,
        .block = prev_block
      };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block)
{
  struct block_search_result find_result = find_good_or_last(block, query);
  if (find_result.type == BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(find_result.block, query);
    find_result.block->is_free = false;
  }
  return find_result;
}

static struct block_header *grow_heap(struct block_header *restrict last, size_t query)
{
  if (!last) {
    return NULL;
  }
  void* addr = block_after(last);
  const struct region region = alloc_region(addr, query);
  if (region_is_invalid(&region)) {
    return NULL;
  }

  last->next = region.addr;
  return try_merge_with_next(last) ? last : last->next;
}

/* get last block */
struct block_header* get_last(struct block_header *heap_start) {
  struct block_header* cur_elem = heap_start;
  while (cur_elem->next) {
    cur_elem = cur_elem->next;
  }
  return cur_elem;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start)
{
  if (!heap_start) {
    return NULL;
  }

  size_t needed_capacity = query >= BLOCK_MIN_CAPACITY ? query : BLOCK_MIN_CAPACITY;
  needed_capacity = (needed_capacity + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
  struct block_search_result result = try_memalloc_existing(needed_capacity, heap_start);
  if (result.type == BSR_FOUND_GOOD_BLOCK) {
    return result.block;
  }

  struct block_header* new_block = grow_heap(get_last(heap_start), needed_capacity);
  if (!new_block) {
    return NULL;
  }
  
  result = try_memalloc_existing(needed_capacity, new_block);
  return result.block;
}

void *_malloc(size_t query)
{
  struct block_header *const addr = memalloc(query, (struct block_header *)HEAP_START);
  if (addr)
    return addr->contents;
  else
    return NULL;
}

static struct block_header *block_get_header(void *contents)
{
  return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

void _free(void *mem)
{
  if (!mem)
    return;
  struct block_header *header = block_get_header(mem);
  header->is_free = true;
  merge_all(header);
}
