#define _DEFAULT_SOURCE

#include <assert.h>

#include "mem_internals.h"
#include "mem.h"

#define MIN_BLOCK_CAPACITY 24
#define BIG_CAPACITY 5000

#define RUN_TEST(test) \
printf(" Run test \"%s\"...", #test); \
heap_init(0); \
printf("%s \n", test()); \
heap_term();

static struct block_header* block_get_header(void *contents)
{
  return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

static struct block_header* get_last(struct block_header *heap_start) {
  struct block_header* cur_elem = heap_start;
  while (cur_elem->next) {
    cur_elem = cur_elem->next;
  }
  return cur_elem;
}


static char* min_malloc_test() {
    uint8_t* contents = _malloc(0);
    struct block_header* header = block_get_header(contents);

    assert(header->capacity.bytes == MIN_BLOCK_CAPACITY);
    assert(header->next != NULL);
    assert(!header->is_free);
    assert(header == HEAP_START);
    
    return "OK";
}

static char* single_malloc_test() {
    uint8_t* contents = _malloc(MIN_BLOCK_CAPACITY + 1);
    struct block_header* header = block_get_header(contents);

    assert(header->capacity.bytes == MIN_BLOCK_CAPACITY + 1);
    assert(header->next != NULL);
    assert(!header->is_free);
    assert(header == HEAP_START);
    
    return "OK";
}

static char* single_free_test() {
    uint8_t* mal1 = _malloc(0);
    uint8_t* mal2 = _malloc(0);

    
    struct block_header* block1 = block_get_header(mal1);
    struct block_header* block2 = block_get_header(mal2);

    _free(mal2);

    assert(block2->is_free);
    assert(block1->next == block2);
    assert(block2->next == NULL);
    
    return "OK";
}


static char* free_test() {
    _malloc(0);
    uint8_t* mal2 = _malloc(0);
    uint8_t* mal3 = _malloc(0);

    
    struct block_header* block2 = block_get_header(mal2);
    struct block_header* block3 = block_get_header(mal2);

    size_t start_capacity = block2->capacity.bytes;

    _free(mal3);
    _free(mal2);
    size_t end_capacity = block2->capacity.bytes;

    assert(block2->is_free);
    assert(block3->is_free);
    assert(block2->next == NULL);
    assert(end_capacity > start_capacity);
    
    return "OK";
}


static char* continues_extend_test() {
    uint8_t* big1 = _malloc(BIG_CAPACITY);
    uint8_t* big2 = _malloc(BIG_CAPACITY);

    struct block_header* block1 = block_get_header(big1);
    struct block_header* block2 = block_get_header(big2);

    assert(block1->next == block2);
    assert((void*)(block1->contents + block1->capacity.bytes) == block2);
    
    return "OK";
}

static char* extend_test() {
    void* gap = mmap(HEAP_START + REGION_MIN_SIZE, 10, 0, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    uint8_t* big1 = _malloc(capacity_from_size((block_size){REGION_MIN_SIZE}).bytes);
    uint8_t* big2 = _malloc(BIG_CAPACITY);

    struct block_header* block1 = block_get_header(big1);
    struct block_header* block2 = block_get_header(big2);

    munmap(gap, 10);
    
    assert(block1->next == block2);
    assert((void*)(HEAP_START + REGION_MIN_SIZE) != block2);
    
    return "OK";
}

int main() {
    RUN_TEST(min_malloc_test);
    RUN_TEST(single_malloc_test);
    RUN_TEST(single_free_test);
    RUN_TEST(free_test);
    RUN_TEST(continues_extend_test);
    RUN_TEST(extend_test);

}